<?php

/**
* Implement hook_rules_action_info().
*
* Define custom Rules action(s).
*/
function sparkpost_rules_action_info() {
  return array(
    'sparkpost_rules_action_send_email' => array(
      'label' => t('Send email (SparkPost)'),
      'group' => t('SparkPost'),
      'parameter' => array(
        'to' => array(
          'type' => 'text',
          'label' => t('To'),
          'description' => t('Email address to send the email to.'),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t('Subject of the email.'),
          'optional' => True,
        ),
        'text_message' => array(
          'type' => 'text',
          'label' => t('Message (text)'),
          'description' => t('The text content of the email message.'),
          'optional' => True,
        ),
        'html_message' => array(
          'type' => 'text',
          'label' => t('Message (HTML)'),
          'description' => t('The HTML content of the email message.'),
          'optional' => True,
        ),
        'campaign' => array(
          'type' => 'text',
          'label' => t('Campaign ID'),
          'description' => t('The campaign ID to use for the email.'),
          'optional' => True,
        ),
        'template' => array(
          'type' => 'text',
          'label' => t('Template ID'),
          'description' => t('The template ID to use for the email.'),
          'optional' => True,
        ),
        'substitution' => array(
          'type' => 'text',
          'label' => t('Substitution data'),
          'description' => t('Any substitution data required by the template. One piece per line, in the form, \'name=AC\''),
          'optional' => True,
        ),
      ),
    ),
  );
}

function sparkpost_rules_action_send_email($to, $subject, $text_message, $html_message = '', $campaign = '', $template = '', $substitution = '') {
  // Concert the substitution data from a string into an array.
  $substitution_array = _sparkpost_convert_substitution_to_array($substitution);
 
 // Send the email.
  sparkpost_send($to, $subject, $text_message, $html_message, $campaign, $template, $substitution_array);
}

