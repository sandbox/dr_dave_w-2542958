<?php

/**
 * @file
 * Module file for SparkPost.
 */


function sparkpost_send($to, $subject, $text_message, $html_message = '', $campaign = '', $template = '', $substitution = '') {
  // Check the required parameters were givenR
  
  // Initialise the returned variables.
  $total_rejected_recipients = '';
  $total_accepted_recipients = '';
  $id = '';

  // Read the module configuration.

  // Get the sender name and email address. Complain it not set.
  $sender_name = variable_get('sparkpost_sender_name', '');
  $sender_email = variable_get('sparkpost_sender_email', '');
  if ($sender_name == '') {
    $comment = "Sender's name must be set in the module config page.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }
  if ($sender_email == '') {
    $comment = "Sender's email address must be set in the module config page.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }

  // See if we should log messages.
  $log_messages = false;
  $log = variable_get("sparkpost_log", false);
  if ($log == 1)
    $log_messages = true;

  // Set the API URL.
  $url = 'https://api.sparkpost.com/api/v1/transmissions';

  // Get the API key.
  $api_key = variable_get("sparkpost_api_key", "");

  // Complain if there is no key.
  if ($api_key == '') {
    $comment = "API key must be given in module config page.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }

  // Set the HTTP headers.
  $headers = array(
    'Content-Type: application/json',
    'Authorization: ' . $api_key,
  );

  //$comment = 'Sending email to "' . $to . '"';
  //if ($log_messages) watchdog('sparkpost', $comment);

  // Construct the data to send.
  $recipients_array = array(
    "address" => array(
      "email" => $to,
      //"tags":["learning"],
      //"substitution_data" => array(
        //"customer_type" => "Platinum",
        //"first_name" => "Dave",
      //),
    ),
  );
  if ($template == '') {
    $content_array = array(
      "from" => array(
        //"name" => variable_get('site_name'),
        "name" => $sender_name,
        //"email" => "testing@sparkpostbox.com",
        "email" => $sender_email,
      ),
      "subject" => $subject,
      "reply_to" => variable_get('site_name'),
      "text" => $text_message,
      "html" => $html_message,
    );
  } else {
    $content_array = array(
      "template_id" => $template,
    );
  }
  $data_obj = new stdClass();
  //$data_obj -> email_rfc822 = $text_message;
  $data_obj -> recipients = array($recipients_array);
  if ($campaign != '') {
    $data_obj -> campaign_id = $campaign;
  }
  $data_obj -> content = $content_array;
  if ($substitution != '') {
    $data_obj -> substitution_data = $substitution;
  }
  //dpm($data_obj);

  // Convert to json.
  $data_json = json_encode($data_obj, JSON_UNESCAPED_SLASHES);

  // Initilaise curl.
  $ch = curl_init($url);

  // Setup the submission.
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  // Submit the data.
  $response = curl_exec($ch);
  //$response = 'Fake';

  // Close the curl.
  curl_close($ch);

  // Parse the response.
  $parsed_response = json_decode($response);

  // Check the response for errors. If there are any, report.
  if (isset($parsed_response -> errors)) {
    foreach($parsed_response -> errors as $error) {
      $error_message = 'Error from API';
      if (isset($error -> message))
        $error_message .= ' - ' .$error -> message;
      if (isset($error -> description))
        $error_message .= ' - ' .$error -> description;
      if (isset($error -> code))
        $error_message .= ' (code ' .$error -> code . ')';
      $error_message .= ' Failed to send email to ' . $to;
      watchdog('sparkpost', $error_message, null, WATCHDOG_ERROR);
    }
  }

  // See if sendng works.
  if (isset($parsed_response -> results)) {
    $results = $parsed_response -> results;
    if (isset($results -> total_rejected_recipients))
      $total_rejected_recipients = $results -> total_rejected_recipients;
    if (isset($results -> total_accepted_recipients))
      $total_accepted_recipients = $results -> total_accepted_recipients;
    if (isset($results -> id))
      $id = $results -> id;
    if ($total_accepted_recipients == '1') {
      $comment = "Email sent to $to, ID=$id.";
      if ($log_messages) watchdog('sparkpost', $comment);
      //drupal_set_message($comment);
    }
  }

  return $id;
}



/* 
 * Get SparkPost metrics.
 */
function sparkpost_metrics($type = 'deliverability', $metric = '', $domain = '', $campaign = '', $template = '', $start = '', $end = '', $precision = '') {
  $add_duration = False;

  // Remember the start time.
  if ($add_duration) $start_time = microtime(True);

  /*// Check the metrics type requested is valid.
  $metrics_types = array(
    'campaigns',
    'deliverability',
    'deliverability/attempt',
    'domains',
  );
  //if (!in_array($type, $metrics_types)) {
    //$comment = "Invalid metrics type requested.";
    //watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    //return '';
  //}*/

  // Set the API URL.
  $url = 'https://api.sparkpost.com/api/v1/metrics/' . $type;

  // Add from/to paramaters if required.
  $params = array();
  if (substr($type, 0, 14) == 'deliverability') {
    if (($start != '') & ($end != '')) {
      $start_time = $start;
      $end_time = $end;
    } else {
      $start_time =  strtotime('-1 day');
      $end_time = time();
    }
    $time_format = 'Y-m-d\TH:i';
    $start_time_string = date($time_format, $start_time);
    $end_time_string = date($time_format, $end_time);
    $params['from'] = $start_time_string;
    $params['to'] = $end_time_string;
    $params['timezone'] = date_default_timezone_get();
  }
  if ($metric != '') {
    $params['metrics'] = $metric;
  }
  if ($domain != '') {
    $params['domains'] = $domain;
  }
  if ($template != '') {
    $params['templates'] = $template;
  }
  if ($campaign != '') {
    $params['campaigns'] = $campaign;
  }
  if ($precision != '') {
    $params['precision'] = $precision;
  }
  if (count($params) > 0) {
    $url .= '?' . http_build_query($params);
  }
  // Get the API key.
  $api_key = variable_get("sparkpost_api_key", "");

  // Complain if there is no key.
  if ($api_key == '') {
    $comment = "API key must be given in module config page.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }

  // Set the HTTP headers.
  $headers = array(
    'Authorization: ' . $api_key,
  );

  // Initilaise curl.
  $ch = curl_init($url);

  // Setup the submission.
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPGET, true);

  // Submit the data.
  $response = curl_exec($ch);
  //$response = 'Fake';

  // Close the curl.
  curl_close($ch);

  // Parse the response.
  $parsed_response = json_decode($response);

  // Record the end time and report how long this took.
  if ($add_duration) {
    $stop_time = microtime(True);
    $duration = $stop_time - $start_time;
    $parsed_response -> duration = sprintf("%.2f", $duration);
    $response = json_encode($parsed_response);
  }
  
  //return $response;
  return $parsed_response;
}


/* 
 * Get SparkPost domain info..
 */
function sparkpost_domain($domain) {
  // Get the API key.
  $api_key = variable_get("sparkpost_api_key", "");

  // Complain if there is no key.
  if ($api_key == '') {
    $comment = "API key must be given in module config page.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }

  // Complain if no domain given,
  if ((!isset($domain)) || ($domain == '')) {
    $comment = "Domain must be given.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }

  // Set the API URL.
  $url = 'https://api.sparkpost.com/api/v1/sending-domains/' . $domain;

  // Set the HTTP headers.
  $headers = array(
    'Authorization: ' . $api_key,
  );

  // Initilaise curl.
  $ch = curl_init($url);

  // Setup the submission.
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPGET, true);

  // Submit the data.
  $response = curl_exec($ch);
  //$response = 'Fake';

  // Close the curl.
  curl_close($ch);

  // Parse the response.
  $parsed_response = json_decode($response);

  return $parsed_response;
}


/* 
 * Get list of SparkPost templates.
 */
function sparkpost_templates() {
  // Get the API key.
  $api_key = variable_get("sparkpost_api_key", "");

  // Complain if there is no key.
  if ($api_key == '') {
    $comment = "API key must be given in module config page.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }

  // Set the API URL.
  $url = 'https://api.sparkpost.com/api/v1/templates';

  // Set the HTTP headers.
  $headers = array(
    'Authorization: ' . $api_key,
  );

  // Initilaise curl.
  $ch = curl_init($url);

  // Setup the submission.
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPGET, true);

  // Submit the data.
  $response = curl_exec($ch);
  //$response = 'Fake';

  // Close the curl.
  curl_close($ch);

  // Parse the response.
  $parsed_response = json_decode($response);
  return $parsed_response;
}



/*
 * Create form for the module config page.
 */
function sparkpost_admin() {
  $form = array();

  // Field for the API key.
  $form['sparkpost_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('SparkPost API Key'),
    '#default_value' => variable_get('sparkpost_api_key', ''),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t("Your API key from SparkPost."),
    '#required' => TRUE,
  );

  // Field for the sender's name.
  $form['sparkpost_sender_name'] = array(
    '#type' => 'textfield',
    '#title' => t("Sender's name"),
    '#default_value' => variable_get('sparkpost_sender_name', ''),
    '#size' => 30,
    '#maxlength' => 100,
    '#description' => t('What name should email be sent from?'),
    '#required' => TRUE,
  );

  // Field for the sender's email.
  $form['sparkpost_sender_email'] = array(
    '#type' => 'textfield',
    '#title' => t("Sender's email address"),
    '#default_value' => variable_get('sparkpost_sender_email', ''),
    '#size' => 30,
    '#maxlength' => 100,
    '#description' => t('What email address should email be sent from? Your SparkPost account must be authorised to send email from this address.'),
    '#required' => TRUE,
  );

  // Field for the log setting.
  $form['sparkpost_log'] = array(
    '#type' => 'checkbox',
    '#title' => t('Write to log?'),
    '#default_value' => variable_get('sparkpost_log', ''),
    '#description' => t("Would you like email requests to be logged?"),
  );

  // Field for the test page.
  $form['sparkpost_test_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable SparkPost test page?'),
    '#default_value' => variable_get('sparkpost_test_page', ''),
    '#description' => t("The SparkPost test page allows you to send emails to the site admin email address to test if the module is working. The page will be at <a href=\"/sparkpost/test\">/sparkpost/test.</a>"),
  );

  return system_settings_form($form);
}


/*
 * Page callback for the SparkPost Metrics page.
 */
function _sparkpost_metrics_page() {
  // Get the API key.
  $api_key = variable_get("sparkpost_api_key", "");

  // Complain if there is no key.
  if ($api_key == '') {
    $comment = "Error retreiving metrics, API key must be given in module config page.";
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return '';
  }
  
  // See what the period is.
  $period = arg(2);
  if ($period == '') {
    $period = 'day';
  }

  // See if a domain was specified.
  if (isset($_GET['domain'])) {
    $domain = $_GET['domain'];
  } else {
    $domain = '';
  }

  // See if a campaign was specified.
  if (isset($_GET['campaign'])) {
    $campaign = $_GET['campaign'];
  } else {
    $campaign = '';
  }

  // See if a template was specified.
  if (isset($_GET['template'])) {
    $template = $_GET['template'];
  } else {
    $template = '';
  }

  // Initilaise the output array.
  $output_array = array();

  // Setp the lists of metrics available.
  $sub_types = array(
    'attempt', 
    'bounce-classification',
    'bounce-reason',
    'bounce-reason/domain',
    'campaign',
    'delay-reason',
    'delay-reason/domain',
    'domain',
    'link-name',
    'rejection-reason',
    'rejection-reason/domain',
    'template',
    //'time-series',  // Skip this one, it requests a lot of data.
    'watched-domain',
  );
  $sub_types_with_metrics = array(
    'campaign',
    'domain',
    'template',
    'time-series',
    'watched-domain',
  );
  $metrics = array(
    'count_accepted',
    'count_admin_bounce',
    'count_block_bounce',
    'count_bounce',
    'count_clicked',
    'count_delayed',
    'count_delayed_first',
    'count_delivered',
    'count_delivered_first',
    'count_delivered_subsequent',
    'count_generation_failed',
    'count_generation_rejection',
    'count_hard_bounce',
    'count_inband_bounce',
    'count_injected',
    'count_outofband_bounce',
    'count_policy_rejection',
    'count_rejected',
    'count_rendered',
    'count_sent',
    'count_soft_bounce',
    'count_spam_complaint',
    'count_targeted',
    'count_undetermined_bounce',
    'count_unique_clicked',
    'count_unique_confirmed_opened',
    'count_unique_rendered',
    'total_delivery_time_first',
    'total_delivery_time_subsequent',
    'total_msg_volume',
  );
  $full_metrics_list = 'count_accepted,count_admin_bounce,count_block_bounce,count_bounce,count_clicked,count_delayed,count_delayed_first,count_delivered,count_delivered_first,count_delivered_subsequent,count_generation_failed,count_generation_rejection,count_hard_bounce,count_inband_bounce,count_injected,count_outofband_bounce,count_policy_rejection,count_rejected,count_rendered,count_sent,count_soft_bounce,count_spam_complaint,count_targeted,count_undetermined_bounce,count_unique_clicked,count_unique_confirmed_opened,count_unique_rendered,total_delivery_time_first,total_delivery_time_subsequent,total_msg_volume';
  $metrics_list = 'count_targeted,count_injected,count_sent,count_accepted,count_delivered,count_rejected,count_bounce,count_spam_complaint,total_msg_volume';

  // Generate the main heading.
  if (($domain == '') && ($campaign == '') && ($template == '')) {
    $main_heading = 'Showing all data';
    $sub_content = 'To show data for a specific domain, campaign or template, click on a link below.';
  } else {
    $main_heading = 'Showing data for';
    $have_added = False;
    if ($domain != '') {
      $main_heading .= ' domain ' . $domain;
      $have_added = True;
     
    }
    if ($campaign != '') {
      if ($have_added) {
        $main_heading .= ' and campaign ' . $campaign;
      } else {
        $main_heading .= ' campaign ' . $campaign;
      }
      $have_added = True;
    }
    if ($template != '') {
      if ($have_added) {
        $main_heading .= ' and template ' . $template;
      } else {
        $main_heading .= ' template ' . $template;
      }
      $have_added = True;
    }
    $sub_content = 'To show all data ' . l('click here', current_path()) .'.';
  }
  $output_array['main_heading'] = array(
    '#type' => 'markup',
    '#markup' => '<h3>' . $main_heading . '</h3>',
  );
  $output_array['sub_content'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . $sub_content . '</p>',
  );
  
  // Add the output for the domains API, the list of domains.
  $output_array['domains'] = array(
    '#type' => 'markup',
    '#markup' => '<h4>Domains</h4>',
  );
  $metrics_response = sparkpost_metrics('domains');
  if (isset($metrics_response -> results)) {
    $metrics_results = $metrics_response -> results;
    foreach ($metrics_results as $set_of_results) {
      $rows = array();
      foreach ($set_of_results as $name => $value) {
        // Get domain info.
        $info = sparkpost_domain($value);
        // Generate a link.
        $url_options = array(
          'query' => array(
            'domain' => $value,
          ),
        );
        $url = url('sparkpost/metrics', $url_options);
        $row = array('#markup' => l(t($value), current_path(), $url_options));
        if (isset($info -> results)) {
          $result = $info -> results;
          if (isset($result -> status)) {
            $status = $result -> status;
            if (isset($status -> ownership_verified)) {
              $row[] = ($status -> ownership_verified == '1') ? 'Yes' : 'No';; 
            } else {
              $row[] = "-";
            }
            if (isset($status -> spf_status)) {
              $row[] = $status -> spf_status;
            } else {
              $row[] = "-";
            }
            if (isset($status -> dkim_status)) {
              $row[] = $status -> dkim_status;
            } else {
              $row[] = "-";
            }
            if (isset($status -> abuse_at_status)) {
              $row[] = $status -> abuse_at_status;
            } else {
              $row[] = "-";
            }
            if (isset($status -> compliance_status)) {
              $row[] = $status -> compliance_status;
            } else {
              $row[] = "-";
            }
            if (isset($status -> postmaster_at_status)) {
              $row[] = $status -> postmaster_at_status;
            } else {
              $row[] = "-";
            }
          }
        } else {
              $row[] = "";
              $row[] = "";
              $row[] = "";
              $row[] = "";
              $row[] = "";
              $row[] = "";
        }
        $rows[] = $row;
      }
    }
    if (count($rows) == 0) {
      $rows[] = array('None');
      $headers = '';
    } else {
      $headers = array(
        'Domain',
        'Ownership verified',
        'SPF',
        'DKIM',
        'Abuse at',
        'Compliance',
        'Postmaster at',
      );
    }
    $output_array[] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $headers,
    );
  }
  
  // Add the output for the campaigns API, the list of campaigns.
  $output_array['campaigns'] = array(
    '#type' => 'markup',
    '#markup' => '<h4>Campaigns</h4>',
  );
  $metrics_response = sparkpost_metrics('campaigns');
  $rows = array();
  if (isset($metrics_response -> results)) {
    $metrics_results = $metrics_response -> results;
    foreach ($metrics_results as $set_of_results) {
      $rows = array();
      foreach ($set_of_results as $name => $value) {
        //$rows[] = array($value);
        // Generate a link.
        $url_options = array(
          'query' => array(
            'campaign' => $value,
          ),
        );
        $url = url('sparkpost/metrics', $url_options);
        $rows[] = array('#markup' => l(t($value), current_path(), $url_options));
      }
    }
    if (count($rows) == 0) {
      $rows[] = array('None');
    }
    $output_array[] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      //'#empty' => 'None',
    );
  }
  // Add the output for the temapltes API, the list of temapltes.
  $output_array['templates'] = array(
    '#type' => 'markup',
    '#markup' => '<h4>Templates</h4>',
  );
  $metrics_response = sparkpost_templates();
  $rows = array();
  if (isset($metrics_response -> results)) {
    $metrics_results = $metrics_response -> results;
    foreach ($metrics_results as $set_of_results) {
      $name = $set_of_results -> name;
      $id = $set_of_results -> id;
      $url_options = array(
        'query' => array(
          'template' => $id,
        ),
      );
      $rows[] = array('#markup' => l(t($name), current_path(), $url_options));
      #$rows[] = array($name);
    }
    if (count($rows) == 0) {
      $rows[] = array('None');
    }
    $output_array[] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      //'#empty' => 'None',
    );
  }


  // Get the deliverability data.
  $title = "Delivery summary";
  $have_added = False;
  if ($domain != '') {
    $title .= ' for domain ' . $domain;
    $have_added = True;
  }
  if ($campaign != '') {
    if ($have_added) {
      $title .= ' and ';
    } else {
      $title .= ' for ';
    }
    $title .= 'campaign ' . $campaign;
    $have_added = True;
  }
  if ($template != '') {
    if ($have_added) {
      $title .= ' and ';
    } else {
      $title .= ' for ';
    }
    $title .= 'template ' . $template;
  }
  $output_array['deliverability'] = array(
    '#type' => 'markup',
    '#markup' => '<h4>' . $title . '</h4>',
  );
  // Work out the start and end times and the precision.
  switch($period) {
    case 'month':
      $start_time_string = date('Y-m-d 00:00:00', strtotime('-1 month'));
      $precision = 'day';
      $date_format = 'D d/m';
      break;
    case 'week':
      $start_time_string = date('Y-m-d 00:00:00', strtotime('-6 days'));
      $precision = 'day';
      $date_format = 'D d/m';
      break;
    case 'day':
    default:
      $start_time_string = date('Y-m-d 00:00:00');
      $precision = 'hour';
      $date_format = 'H:00';
  }
  $start_time = strtotime($start_time_string);

  // Call the API to get the data.
  $metrics_response = sparkpost_metrics('deliverability/time-series', $metrics_list, $domain, $campaign, $template, $start_time, time(), $precision);
  // Convert the output.
  $rows = array();
  if (isset($metrics_response -> results)) {
    $metrics_results = $metrics_response -> results;
    $results_array = array();
    foreach ($metrics_results as $set_of_results) {
      $ts  = $set_of_results -> ts;
      unset($set_of_results -> ts);
      $day = date($date_format, strtotime($ts));
      //$results_array[$day] = array();
      




      foreach ($set_of_results as $name => $value) {
        if (($name != 'domain') && ($name != 'campaign')) {
          //$results_array[$day][$name] = $value;
          $results_array[$name][$day] = $value;
          //$rows[] = array($name, $value);
        }
      }
    }
    // Initialiase the heaers array and add a blank header for the first column.
    $headers = array();
    $headers[] = 'Metric';
    $have_headers = False;
    if (count($results_array) > 0) {
      foreach($results_array as $name => $days) {
        $row = array();
        $row[] = $name;
        foreach($days as $day => $value) {
          if (!$have_headers) {
            $headers[] = $day;
          }
          $row[] = $value;
        }
        $have_headers = True;
        $rows[] = $row;
      }
    }
    if (count($rows) == 0) {
      $rows[] = array('None');
    }
    $output_array[] = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    );
  }


  //return $response;
  return $output_array;
}

/*
 * Create form for the SparkPost testpage.
 */
function sparkpost_test_form() {
  $form = array();

  // Check if the page is enabled.
  $page_enabled = variable_get("sparkpost_test_page", false);
  if ($page_enabled != '1') {
    drupal_goto('/');
    return "";
  }

  // Field for the campaign.
  $form['campaign'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign'),
    '#default_value' => '',
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t("Optionally provide a campaign ID."),
    '#required' => FALSE,
  );

  // Field for the template.
  $form['template'] = array(
    '#type' => 'textfield',
    '#title' => t('Template'),
    '#default_value' => '',
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t("Optionally provide a template ID. The ID given must match one you have created on SparkPost."),
    '#required' => FALSE,
  );

  // Field for the email address.
  $form['sparkpost_test_to'] = array(
    '#type' => 'textfield',
    '#title' => t('To email address'),
    '#default_value' => variable_get('site_mail', ''),
    '#size' => 50,
    '#maxlength' => 100,
    '#description' => t("Using this test page, you can only send an email to the site admin email address."),
    '#disabled' => TRUE,
    '#required' => TRUE,
  );

  // Field for the subject.
  $form['sparkpost_test_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => '',
    '#size' => 50,
    '#maxlength' => 100,
    '#required' => FALSE,
  );

  // Field for the message.
  $form['sparkpost_test_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => '',
    '#rows' => 3,
    '#required' => FALSE,
  );

  // Field for the substitution data.
  $form['sparkpost_substitution'] = array(
    '#type' => 'textarea',
    '#title' => t('Substitution data'),
    '#default_value' => '',
    '#rows' => 3,
    '#required' => FALSE,
  );

  // Add the submit button.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send email',
  );

  return $form;
}

/* 
 * Function to handle test page form submission.
 */
function sparkpost_test_form_submit($form_id, $form_values) {

  // If the test page is not enabled, complain.
  if (variable_get("sparkpost_test_page", false) != '1') {
    $comment = 'SparkPost test page is not enabled.';
    watchdog('sparkpost', $comment, null, WATCHDOG_ERROR);
    return;
  }

  if (isset($form_values['values']['sparkpost_test_to']) && 
      isset($form_values['values']['sparkpost_test_subject']) &&
      isset($form_values['values']['sparkpost_test_message'])) {
    $to = $form_values['values']['sparkpost_test_to'];
    $subject = $form_values['values']['sparkpost_test_subject'];
    $html_message = '<p>' . $form_values['values']['sparkpost_test_message'] . '</p>';
    $text_message = $form_values['values']['sparkpost_test_message'];
    if (isset($form_values['values']['campaign'])) {
      $campaign = $form_values['values']['campaign'];
    } else {
      $campaign = '';
    }
    if (isset($form_values['values']['template'])) {
      $template = $form_values['values']['template'];
    } else {
      $template = '';
    }
    // Process the substitution data.
    $substitution = '';
    if (isset($form_values['values']['sparkpost_substitution']) && (strlen($form_values['values']['sparkpost_substitution']) > 0)) {
      $substitution = _sparkpost_convert_substitution_to_array($form_values['values']['sparkpost_substitution']);
    }
    $response = sparkpost_send($to, $subject, $text_message, $html_message, $campaign, $template, $substitution);
    if (($response != '') && (is_numeric($response))) {
      drupal_set_message("Email sent to $to, ID=$response.");
    } else {
      drupal_set_message("Failed to send email to $to, check the <a href=\"/admin/reports/dblog\">recent log messages</a>.", 'error');
    }
  }
}


function _sparkpost_convert_substitution_to_array($substitution_string) {
  // Process the substitution data.
  $substitution = array();
  if (strlen($substitution_string) > 0) {
    // Split the substituion data into separate lines.
    $e = explode("\n", $substitution_string);
    // Loop through each line.
    foreach($e as $sub) {
      // Try and split the line at the equals sign.
      $ee = explode("=", $sub);
      // If splitting works and there are just 2 parts to the string...
      if (count($ee) == 2) {
        $key = trim($ee[0]);
        $value = trim($ee[1]);
        // If the data is not empty, add it to the substitution array.
        if ((strlen($key) > 0) && (strlen($value) > 0)) {
          $substitution[$key] = $value;
        }
      }
    }
  }
  if (count($substitution) > 0) {
    return $substitution;
  } else {
    return '';
  }
}




/**
 * Implements hook_menu().
 */
function sparkpost_menu() {
  $items = array();

  // Module configuration page.
  $items['admin/config/system/sparkpost'] = array(
    'title' => 'SparkPost module settings',
    'description' => 'Configuration page for the SparkPost module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sparkpost_admin'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
   );

  // SparkPost module metrics page.
  $items['sparkpost/metrics'] = array(
    'title' => 'SparkPost Metrics',
    'description' => 'Retreives metrics information from Sparkost.',
    'page callback' => '_sparkpost_metrics_page',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    //'type' => MENU_DEFAULT_LOCAL_TASK,
   );
  $items['sparkpost/metrics/day'] = array(
    'title' => 'Today',
    'description' => 'Retreives metrics information from Sparkost.',
    'page callback' => '_sparkpost_metrics_page',
    'access arguments' => array('administer site configuration'),
    //'type' => MENU_NORMAL_ITEM,
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 10,
   );
  $items['sparkpost/metrics/week'] = array(
    'title' => 'Week',
    'description' => 'Retreives metrics information from Sparkost.',
    'page callback' => '_sparkpost_metrics_page',
    'access arguments' => array('administer site configuration'),
    //'type' => MENU_NORMAL_ITEM,
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
   );
  $items['sparkpost/metrics/month'] = array(
    'title' => 'Month',
    'description' => 'Retreives metrics information from Sparkost.',
    'page callback' => '_sparkpost_metrics_page',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 30,
   );

  // SparkPost module test page.
  $items['sparkpost/test'] = array(
    'title' => 'SparkPost Test Page',
    'description' => 'Test page for SparkPost module testing',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sparkpost_test_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
   );

  return $items;
}


/**
 * Helper function to wrap a string in <p> tags and add a newline.
 */
function _sparkpost_wrap_p($string){
  return "<p>" . $string . "</p>\n";
}

/**
 * Modify the Drupal mail system to use SparkPost to send emails.
 */
class SparkPostMailSystem implements MailSystemInterface {
    public function format(array $message) {
      $comment = __FUNCTION__;
      //dpm($message);
      $message['SparkPost'] = 'yes';
      //$sparkpost_mail = new MimeMailSystem();
      $sparkpost_mail = new DefaultMailSystem();
      $message = $sparkpost_mail->format($message);
      //dpm($message);
      return $message;
    }

    public function mail(array $message) {
      $comment = __FUNCTION__;
      //dpm($message);
      $response = sparkpost_send(
        $message['to'],
        $message['subject'],
        $message['body'],
        $message['body']
      );
      if (($response != '') && (is_numeric($response))) {
        return True;
      } else {
        return False;
      }
    }
  
}


function sparkpost_preprocess_page(&$variables) {
  // Set the title on our pages.
  switch (current_path()) {
    case 'sparkpost/metrics' :
    case 'sparkpost/metrics/day' :
    case 'sparkpost/metrics/week' :
    case 'sparkpost/metrics/month' :
      $title = 'SparkPost Metrics';
      $variables['title'] = $title;
      drupal_set_title($title);
      break;
  }
}
